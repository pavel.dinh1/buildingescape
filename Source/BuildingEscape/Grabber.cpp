// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"

// Does nothing, but improves read-ability of function params
#define OUT 

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	Reach = 200.f;

	//PhysicsHandleComp = CreateDefaultSubobject<UPhysicsHandleComponent>(TEXT("PhysicsHandle"));
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	FindPhysicsHandle();
	SetupInputComponent();
}

/*
* Checking for UserInput component to bind a function when pressed "Grab"
* Make sure to Setup the input in ProjectSettings !
*/
void UGrabber::SetupInputComponent()
{
	UInputComponent* InputComponent = GetOwner()->InputComponent;
	if (InputComponent)
	{
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Drop);
	}
}

/*
* This is good check if the component is added explicitly.
* Better version is to create the component in class
and CreateDefaultSubobject. It saves null pointer checking.
* Checkout the constructor -> PhysicsHandleComp
*/
void UGrabber::FindPhysicsHandle()
{
	PhysicsHandleComp = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

	if (!PhysicsHandleComp)
	{
		UE_LOG(LogTemp, Warning, TEXT("No Physics Handle Component found ! Attach/Add it in the class ! "));
	}
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PhysicsHandleComp) { return; }
	
	// If the physics handle is attached
	if (PhysicsHandleComp->GrabbedComponent)
	{
		// Move the object we are holding.
		PhysicsHandleComp->SetTargetLocation(GetPlayersReach());
	}
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() const
{
	FHitResult Hit;
	FCollisionQueryParams QueryParams(FName(""), false, GetOwner());
	
	GetWorld()->LineTraceSingleByObjectType
	(
		OUT Hit,
		GetPlayersWorldPosition(),
		GetPlayersReach(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		QueryParams
	);

	return Hit;
}

void UGrabber::Grab()
{	
	FHitResult Hit = GetFirstPhysicsBodyInReach();
	AActor* HitActor = Hit.GetActor();

	// If we hit something, then attach the physics handle.
	if (HitActor) 
	{
		if (!PhysicsHandleComp) { return; }

		PhysicsHandleComp->GrabComponentAtLocation
		(
			Hit.GetComponent(),
			NAME_None,
			GetPlayersReach()
		);
	}
}

void UGrabber::Drop()
{
	if (!PhysicsHandleComp) { return; }
	PhysicsHandleComp->ReleaseComponent();
}

FVector UGrabber::GetPlayersReach() const
{
	FVector PlayerViewPointLoc;
	FRotator PlayerViewPointRot;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint
	(
		OUT PlayerViewPointLoc,
		OUT PlayerViewPointRot
	);

	return PlayerViewPointLoc + PlayerViewPointRot.Vector() * Reach;
}

FVector UGrabber::GetPlayersWorldPosition() const
{
	FVector PlayerViewPointLoc;
	FRotator PlayerViewPointRot;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint
	(
		OUT PlayerViewPointLoc,
		OUT PlayerViewPointRot
	);
	return PlayerViewPointLoc;
}