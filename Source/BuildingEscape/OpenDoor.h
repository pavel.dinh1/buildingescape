// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "OpenDoor.generated.h"

class ATriggerVolume;
class UAudioComponent;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UOpenDoor();

private:
	float InitialAngle;
	float CurrentAngle;
	float DoorLastOpened;
	float DoorClosedDelay;

	bool bDoorOpened;
	bool bDoorClosed;

	UAudioComponent* AudioComp;

	UPROPERTY(EditAnywhere)
		float MassToOpenDoor;
	UPROPERTY(EditAnywhere)
		float OpenAngle;
	UPROPERTY(EditAnywhere)
		float OpenSpeed;
	UPROPERTY(EditAnywhere)
		float CloseSpeed;
	UPROPERTY(EditAnywhere)
		ATriggerVolume* PressurePlate;
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void OpenDoor(float DeltaTime);
	void CloseDoor(float DeltaTime);
	float TotalMassOfActors() const;
	
	void PlayDoorSound() const;
	void FindAudioComponent();
};
