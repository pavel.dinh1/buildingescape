// Fill out your copyright notice in the Description page of Project Settings.


#include "OpenDoor.h"
#include "Components/PrimitiveComponent.h"
#include "Components/AudioComponent.h"
#include "Engine/TriggerVolume.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	OpenAngle = 90.f;
	DoorClosedDelay = 0.5;
	DoorLastOpened = 0.f;
	OpenSpeed = 0.8f;
	CloseSpeed = 2.f;
	MassToOpenDoor = 50.f;

	bDoorOpened = false;
	bDoorClosed = true;

	PressurePlate = nullptr;
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	if (!PressurePlate)
		UE_LOG(LogTemp, Warning, TEXT("The %s has the open door component, but no pressure plate set !"), *GetOwner()->GetName());

	FindAudioComponent();

	InitialAngle = GetOwner()->GetActorRotation().Yaw;
	CurrentAngle = InitialAngle;
	OpenAngle += InitialAngle;
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	 
	if (TotalMassOfActors() > MassToOpenDoor)
	{
		OpenDoor(DeltaTime); 
		DoorLastOpened = GetWorld()->GetTimeSeconds();
	}
	else
	{
		if(GetWorld()->GetTimeSeconds() - DoorLastOpened > DoorClosedDelay)
			CloseDoor(DeltaTime);
	}
}

void UOpenDoor::OpenDoor(float DeltaTime)
{
	CurrentAngle = FMath::Lerp(CurrentAngle, OpenAngle, DeltaTime * OpenSpeed);
	FRotator DoorRotation = GetOwner()->GetActorRotation();
	DoorRotation.Yaw = CurrentAngle;
	GetOwner()->SetActorRotation(DoorRotation);

	bDoorClosed = false;
	if (!bDoorOpened)
	{
		PlayDoorSound();
		bDoorOpened = true;
	}
}

void UOpenDoor::CloseDoor(float DeltaTime)
{
	CurrentAngle = FMath::Lerp(CurrentAngle, InitialAngle, DeltaTime * OpenSpeed);
	FRotator DoorRotation = GetOwner()->GetActorRotation();
	DoorRotation.Yaw = CurrentAngle;
	GetOwner()->SetActorRotation(DoorRotation);

	bDoorOpened = false;
	if (!bDoorClosed)
	{
		PlayDoorSound();
		bDoorClosed = true;
	}
}

float UOpenDoor::TotalMassOfActors() const
{
	float TotalMass = 0.f;

	// Find all overlapping actors.
	TArray<AActor*> OverlappingActors;

	if (!PressurePlate) { return TotalMass; }
	PressurePlate->GetOverlappingActors(OverlappingActors);

	// Add Up their masses.
	for (AActor* Actor : OverlappingActors)
	{
		TotalMass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}
	return TotalMass;
}

void UOpenDoor::PlayDoorSound() const
{
	if (!AudioComp) { return; }
	AudioComp->Play();
}

// This function checks whether the Owner has Audio component and set it to AudioComp
void UOpenDoor::FindAudioComponent()
{
	AudioComp = GetOwner()->FindComponentByClass<UAudioComponent>();

	if (!AudioComp)
	{
		UE_LOG(LogTemp, Warning, TEXT("Audio Comp not found ! Create it or add it to Owner !"));
	}
}