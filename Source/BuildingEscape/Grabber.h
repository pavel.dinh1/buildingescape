// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Grabber.generated.h"

class UPhysicsHandleComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void SetupInputComponent();
	void FindPhysicsHandle();
	void Grab();
	void Drop();

	// Return the first actor withing reach with physics body
	FHitResult GetFirstPhysicsBodyInReach() const;

	// Return The line trace end
	FVector GetPlayersReach() const;
	
	// Get Players World Position 
	FVector GetPlayersWorldPosition() const;
	
private:
	UPROPERTY(EditAnywhere)
	float Reach;
	UPhysicsHandleComponent* PhysicsHandleComp;
};